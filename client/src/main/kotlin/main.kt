import kotlinx.browser.document
import kotlinx.browser.window
import org.w3c.xhr.XMLHttpRequest
import react.dom.render
import kotlin.js.Date

fun main() {
    window.onload = {
        val selector = document.getElementById("main-block-selector")
        if (selector != null) {
            render(selector) {
                selector {
                    selectHandler = fun(value: String) {
                        val http = XMLHttpRequest()
                        http.open("GET", "/api/timer/create?value=${encodeURIComponent(value)}")
                        http.onload = {
                            if (http.status in 200..399) {
                                window.location.replace(http.responseText)
                            }
                        }
                        http.send()
                    }
                }
            }
        }
        val countdown = document.getElementById("main-block-countdown")
        if (countdown != null) {
            render(countdown) {
                countdown {
                    finishTime = Date(document.getElementById("data")!!.innerHTML)
                }
            }
        }
    }
}

external fun encodeURIComponent(s: String): String
