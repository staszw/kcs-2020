import kotlinx.browser.window
import kotlinx.html.classes
import react.*
import react.dom.p
import kotlin.js.Date
import kotlin.math.floor

external interface TimerProps : RProps {
    var finishTime: Date
}

external interface TimerState : RState {
    var currentTime: Date
    var isOver: Boolean
}

class TimerComponent : RComponent<TimerProps, TimerState>() {
    private var interval: Int = -1
    override fun RBuilder.render() {
        p {
            attrs {
                classes = setOf("time")
            }
            +timerText()
        }
    }

    private fun getDifference(state: TimerState) : Int {
        val difference = props.finishTime.getTime() - state.currentTime.getTime()
        return floor(difference / 1000).toInt()
    }

    private fun timerText(): String {
        return if (state.currentTime == undefined) {
            "Loading..."
        } else if (state.isOver) {
            "Time's up"
        } else {
            executeTimer(getDifference(state))
        }
    }

    override fun componentDidMount() {
        interval = window.setInterval(
            {
                setState {
                    currentTime = Date()
                    if (getDifference(this) <= 0) {
                        isOver = true
                        window.clearInterval(interval)
                    }
                }
            }, 1000
        )
    }
}

fun executeTimer(timeLeft: Int): String {
    val seconds = timeLeft % 60
    val minutes = timeLeft / 60 % 60
    val hours = timeLeft / 60 / 60 % 24
    val days = timeLeft / 60 / 60 / 24
    return (if (days > 0) "$days days " else "") +
            (if (hours > 0) "$hours hours " else "") +
            (if (minutes > 0) "$minutes minutes " else "") +
            (if (seconds > 0) "$seconds seconds " else "") + "left"
}

fun RBuilder.timer(handler: TimerProps.() -> Unit): ReactElement {
    return child(TimerComponent::class) {
        this.attrs(handler)
    }
}
