import kotlinx.browser.window
import react.*
import react.dom.div
import react.dom.p
import kotlin.js.Date

external interface CountdownProps : RProps {
    var finishTime: Date
}

class CountdownComponent : RComponent<CountdownProps, RState>() {
    private fun sharable(): Boolean {
        val year = 365.0 * 24 * 60 * 60 * 1000
        return props.finishTime.getTime() - Date().getTime() <= year
    }

    override fun RBuilder.render() {
        div {
            if (props.finishTime == undefined) {
                p("time") {
                    +"Loading..."
                }
            } else if (!sharable()) {
                p("time") {
                    +"Set real goals for yourself -- Albert Einstein"
                }
            } else {
                timer {
                    finishTime = props.finishTime
                }
                shareAndCopy {
                    link = window.location.href
                }
            }
        }
    }
}


fun RBuilder.countdown(handler: CountdownProps.() -> Unit): ReactElement {
    return child(CountdownComponent::class) {
        this.attrs(handler)
    }
}
