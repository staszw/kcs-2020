import kotlinx.browser.document
import kotlinx.html.ButtonType
import kotlinx.html.InputType
import kotlinx.html.classes
import kotlinx.html.id
import kotlinx.html.js.onClickFunction
import org.w3c.dom.HTMLInputElement
import react.*
import react.dom.button
import react.dom.div
import react.dom.input
import react.dom.span

external interface ShareAndCopyProps : RProps {
    var link : String
}
external interface ShareAndCopyState : RState {
    var clicked : Boolean
}
class ShareAndCopyComponent : RComponent<ShareAndCopyProps, ShareAndCopyState>() {
    private fun onClickShare() {
        setState {
            clicked = true
        }
    }
    private fun onClickCopy() {
        (document.getElementById("shareable-input") as HTMLInputElement).apply {
            select()
            setSelectionRange(0, 99999)
        }
        document.execCommand("copy")
    }
    override fun RBuilder.render() {
        if (!state.clicked) {
            button {
                attrs {
                    classes = setOf("share-button", "button")
                    type = ButtonType.submit
                    onClickFunction = { onClickShare() }
                }
                span { +"Share!" }
            }
        } else {
            div {
                input {
                    attrs {
                        id = "shareable-input"
                        classes = setOf("url-input")
                        type = InputType.url
                        value = if (props.link == undefined) "TODO" else props.link
                        readonly = true
                    }
                }
                button {
                    attrs {
                        classes = setOf("copy-button", "button")
                        type = ButtonType.submit
                        onClickFunction = { onClickCopy() }
                    }

                    span { +"Copy" }
                }
            }
        }
    }
}

fun RBuilder.shareAndCopy (handler: ShareAndCopyProps.() -> Unit) : ReactElement {
    return child(ShareAndCopyComponent::class) {
        this.attrs(handler)
    }
}
