import kotlinx.browser.document
import kotlinx.browser.window
import kotlinx.html.ButtonType
import kotlinx.html.InputType
import kotlinx.html.classes
import kotlinx.html.id
import kotlinx.html.js.onClickFunction
import org.w3c.dom.HTMLInputElement
import react.*
import react.dom.button
import react.dom.div
import react.dom.input
import react.dom.span

external interface SelectorProps : RProps {
    var selectHandler: (String) -> Unit
}

class SelectorComponent : RComponent<SelectorProps, RState>() {

    private fun getDate() : String {
        return (document.getElementById("select-date") as HTMLInputElement).value
    }
    private fun getTime() : String {
        return (document.getElementById("select-time") as HTMLInputElement).value
    }
    fun getValue(): String {
        return getDate() + " " + getTime()
    }

    override fun RBuilder.render() {
        div {
            span {
                input {
                    attrs {
                        id = "select-date"
                        classes = setOf("select-date")
                        type = InputType.date
                    }
                }
                input {
                    attrs {
                        id = "select-time"
                        classes = setOf("select-time")
                        type = InputType.time
                    }
                }
            }
            button {
                attrs {
                    classes = setOf("selector-button", "button")
                    type = ButtonType.submit

                    onClickFunction = {
                        if (getDate() == "") {
                            window.alert("Please, choose date")
                        } else if (getTime() == "") {
                            window.alert("Please, choose time")
                        } else {
                            props.selectHandler(getValue())
                        }
                    }
                }
                span {
                    +"Go!"
                }
            }
        }
    }
}

fun RBuilder.selector(handler: SelectorProps.() -> Unit): ReactElement {
    return child(SelectorComponent::class) {
        this.attrs(handler)
    }
}
