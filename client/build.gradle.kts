plugins {
    kotlin("js")
}

group = "org.example"
version = "1.0-SNAPSHOT"

dependencies {
    implementation(kotlin("stdlib-js"))
    implementation("org.jetbrains.kotlinx:kotlinx-html-js:0.7.2")

    //React, React DOM + Wrappers
    implementation("org.jetbrains:kotlin-react:16.13.1-pre.110-kotlin-1.4.0")
    implementation("org.jetbrains:kotlin-react-dom:16.13.1-pre.110-kotlin-1.4.0")
    implementation(npm("react", "16.14.0"))
    implementation(npm("react-dom", "16.14.0"))
}

kotlin {
    js {
        browser {}
        binaries.executable()
    }
}
