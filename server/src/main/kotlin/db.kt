import java.io.FileInputStream
import java.sql.*
import java.util.*

class DB {
    private val TABLE_NAME = "tbl"
    private val DB_NAME = "chronum"
    private val DB_PATH : String
    private val DB_PORT : String
    private val DB_USERNAME : String
    private val DB_PASSWORD : String

    private lateinit var connection: Connection

    init {
        val properties = Properties()
        properties.load(FileInputStream("db.properties"))
        DB_PATH = properties.getProperty("DB_PATH")
        DB_PORT = properties.getProperty("DB_PORT")
        DB_USERNAME = properties.getProperty("DB_USERNAME")
        DB_PASSWORD = properties.getProperty("DB_PASSWORD")
        try {
            // create a connection to the database
            connection = DriverManager.getConnection("jdbc:mysql://$DB_PATH:$DB_PORT/$DB_NAME", DB_USERNAME, DB_PASSWORD)
            println("Connection to MySQL has been established.")

            createTable(connection)
            println("Table created")
        } catch (e: SQLException) {
            println(e.message)
            e.printStackTrace()
        }
    }

    private fun createTable(connection: Connection) {
        val stmt = connection.createStatement()
        stmt.execute("""
        CREATE TABLE IF NOT EXISTS $TABLE_NAME (
            date TEXT NOT NULL,
            `key` TEXT NOT NULL
        )
    """.trimIndent())
    }

    private fun checkConnection() {
        if (connection.isClosed) {
            println("Connection to MySQL is closed.")
            try {
                connection =
                    DriverManager.getConnection("jdbc:mysql://$DB_PATH:$DB_PORT/$DB_NAME", DB_USERNAME, DB_PASSWORD)
                println("Connection to MySQL has been established.")
            } catch (e: SQLException) {
                println(e.message)
                e.printStackTrace()
            }
        }
    }

    fun getDateByKey(key: String): String? {
        checkConnection()
        val stmt: Statement = connection.createStatement()
        val rs: ResultSet = stmt.executeQuery("SELECT date FROM $TABLE_NAME WHERE `key`=\"$key\"")
        while (rs.next()) {
            val encodedUrl = rs.getString("date")
            return String(Base64.getDecoder().decode(encodedUrl))
        }
        return null
    }

    fun getOrCreateKey(date: String, generateKey: () -> String): String {
        val encodedDate = String(Base64.getEncoder().encode(date.toByteArray()))
        checkConnection()
        val stmt: Statement = connection.createStatement()
        val rs: ResultSet = stmt.executeQuery("SELECT `key` FROM $TABLE_NAME WHERE date=\"$encodedDate\"")
        while (rs.next()) {
            return rs.getString("key")
        }

        val key = generateKey()

        stmt.execute("INSERT INTO $TABLE_NAME (date, `key`) VALUES (\"$encodedDate\", \"$key\")")

        return key
    }
}