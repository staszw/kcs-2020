import io.ktor.application.*
import io.ktor.html.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.routing.get
import kotlinx.html.*
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.random.Random
import kotlin.random.nextUInt

fun Application.main() {
    routing {
        suspend fun commonStructure(call: ApplicationCall, idMain: String, innerHtml: DIV.() -> Unit) {
            call.respondHtml {
                head {
                    title {
                        +"Chronum"
                    }

                    link {
                        href = "static/favicon.png"
                        rel = "icon"
                    }
                    link {
                        href = "https://fonts.googleapis.com/css?family=Fira+Sans:300,400,600&display=swap"
                        rel = "stylesheet"
                    }
                    link {
                        href = "static/css/chronum.css"
                        rel = "stylesheet"
                    }
                    script {
                        src = "client.js"
                    }
                }
                body {
                    div("main-block") {
                        id = "main-block-$idMain"
                        innerHtml.invoke(this)
                    }
                    div("footer-block") {
                        span("footer-text") {
                            +"Chronum"
                        }
                        span("footer-muted-text") {
                            +"by staszw"
                        }
                    }
                }
            }
        }
        get("/") {
            commonStructure(call, "selector") {}
        }
        static("/") {
            resources("/")
        }


        val datebase = DB()

        get("/r") {
            try {
                val key = call.parameters["k"]!!
                val date = datebase.getDateByKey(key)!!
                commonStructure(call, "countdown") {
                    div {
                        id = "data"
                        visible = false
                        +date
                    }
                }
            } catch (e: Exception) {
                commonStructure(call, "error") {
                    div("time") {
                        +"404 Not found"
                    }
                }
            }
        }

        fun check(date: String): Boolean {
            return try {
                val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
                LocalDateTime.parse(date, formatter)
                true
            } catch (e: Exception) {
                false
            }
        }

        get("/api/timer/create") {
            val date = call.parameters["value"]!!
            if (!check(date)){
                call.respond(HttpStatusCode.BadRequest, "Invalid date: $date")
                return@get
            }
            val response = datebase.getOrCreateKey(date) {
                Random.nextUInt().toString(16)
            }
            println("Mapped date $date to $response")
            call.respondText("http://" + call.request.host() + "/r?k=" + response)
        }
    }
}
